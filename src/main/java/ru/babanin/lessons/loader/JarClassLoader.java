package ru.babanin.lessons.loader;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Created by makcim on 15.02.17.
 */
public class JarClassLoader extends ClassLoader {
    // https://gitlab.com/pqdn13/edy_files/raw/master/Animal.jar
    private String jarUrlPath = "https://gitlab.com/pqdn13/edy_files/raw/master/Animal.jar";
    private String jarFileName = "Animal.jar";

    private String jarFile = "lib/Animal.jar"; //Path to the jar file
    private Hashtable classes = new Hashtable(); //used to cache already defined classes

    public JarClassLoader() {
        super(JarClassLoader.class.getClassLoader()); //calls the parent class loader's constructor
    }

    public Class loadClass(String className) throws ClassNotFoundException {
        return findClass(className);
    }

    public void copyJarFromUrl(String file, String urlAddress) {
        if (!Files.exists(Paths.get("lib/" + file))) {
            URL url = null;
            try {
                url = new URL(urlAddress);
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return;
            }

            try (FileOutputStream fout = new FileOutputStream("lib/" + file);
                 InputStream fin = url.openStream()) {
                byte[] buf = new byte[512];
                while (true) {
                    int len = fin.read(buf);
                    if (len == -1) {
                        break;
                    }
                    fout.write(buf, 0, len);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public Class findClass(String className) {
        byte classByte[];
        Class result = null;

        result = (Class) classes.get(className); //checks in cached classes
        if (result != null) {
            return result;
        }

        try {
            return findSystemClass(className);
        } catch (Exception e) {
        }

        copyJarFromUrl(jarFileName, jarUrlPath);

        try {
            JarFile jar = new JarFile(jarFile);
            Enumeration<JarEntry> entries = jar.entries();
            JarEntry anEntry;
            JarEntry entry = jar.getJarEntry(className + ".class");
            if (entry == null) while (entries.hasMoreElements()) {
                anEntry = entries.nextElement();
                if (!anEntry.isDirectory()) {
                    String[] dirsAndName = anEntry.getName().split("/");
                    String name = dirsAndName[dirsAndName.length-1];
                    if (name.equals(className + ".class")) {
                        entry = anEntry;
                    }
                }
            }
            InputStream is = jar.getInputStream(entry);
            ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
            int nextValue = is.read();
            while (-1 != nextValue) {
                byteStream.write(nextValue);
                nextValue = is.read();
            }

            String classAndPack = entry.getName();
            classAndPack = classAndPack.replace('.', ' ');
            classAndPack = classAndPack.replace('/', '.');
            String classPath = classAndPack.split(" ")[0];

            classByte = byteStream.toByteArray();

            result = defineClass(classPath, classByte, 0, classByte.length);
            classes.put(className, result);
            return result;
        } catch (Exception e) {
            return null;
        }
    }

}

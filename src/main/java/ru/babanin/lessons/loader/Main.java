package ru.babanin.lessons.loader;

import java.util.SimpleTimeZone;

/**
 * Created by makcim on 15.02.17.
 */

class Mydefwrg{
    private int a = 5;
    private int b = 10;
}

public class Main {



    public static void main(String[] args) {
        System.out.println("hello world");

        JarClassLoader loader = new JarClassLoader();

        Class classAnimal = null;
        try {
            classAnimal = loader.loadClass("ru/babanin/animals/Animal");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        System.out.println("try create class " + classAnimal);

        if (classAnimal != null) {
            try {
                Object animal = classAnimal.newInstance();
                try {
                    XmlSaver.toXml(new Mydefwrg(), System.out);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }


        }


    }
}

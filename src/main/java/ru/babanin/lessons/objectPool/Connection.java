package ru.babanin.lessons.objectPool;

/**
 * Created by makcim on 07.03.17.
 */
public class Connection {
    private static int count = 0;
    private int thisCount;

    public Connection() {
        thisCount = ++count;
    }

    public int getThisCount() {
        return thisCount;
    }
}


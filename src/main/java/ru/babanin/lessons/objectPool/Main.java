package ru.babanin.lessons.objectPool;

/**
 * Created by makcim on 07.03.17.
 */
public class Main {
    public static void main(String[] args) {
        ObjectPool pool = ObjectPool.Holder.getInstance();


        try (ConnectionReusable connection1 = pool.getConnection()){
            connection1.doSomeThing();
        } catch (Exception e) {
            System.out.println("omg dmg");
        }

        try (ConnectionReusable connection1 = pool.getConnection()){
            connection1.doSomeThing();
        } catch (Exception e) {
            System.out.println("omg dmg");
        }

        try (ConnectionReusable connection1 = pool.getConnection()){
            connection1.doSomeThing();
        } catch (Exception e) {
            System.out.println("omg dmg");
        }

        try (ConnectionReusable connection1 = pool.getConnection()){
            connection1.doSomeThing();
        } catch (Exception e) {
            System.out.println("omg dmg");
        }

        try (ConnectionReusable connection1 = pool.getConnection()){
            connection1.doSomeThing();
        } catch (Exception e) {
            System.out.println("omg dmg");
        }

    }
}

package ru.babanin.lessons.objectPool;

public abstract class ConnectionReusable implements AutoCloseable{
    public abstract void doSomeThing();
}

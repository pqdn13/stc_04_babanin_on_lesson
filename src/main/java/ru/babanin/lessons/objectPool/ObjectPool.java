package ru.babanin.lessons.objectPool;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by makcim on 07.03.17.
 */
public class ObjectPool{
    public static class Holder{
        public static final ObjectPool pool = new ObjectPool();
        public static ObjectPool getInstance(){
            return pool;
        }
    }

    private final int size = 2;
    private final List<Connection> passive = new ArrayList<>(size);

    private ObjectPool() {
        for (int i = 0; i < size; i++) {
            passive.add(new Connection());
        }
    }

    public synchronized ConnectionReusable getConnection(){
        ConnectionReusablePool reusablePool = new ConnectionReusablePool();
        return reusablePool.connection != null ? reusablePool : null;
    }

    private class ConnectionReusablePool extends ConnectionReusable{
        Connection connection = null;

        ConnectionReusablePool() {
            if(!passive.isEmpty()){
                connection = passive.remove(passive.size() - 1);
            }
        }

        @Override
        public void close() throws Exception {
            passive.add(connection);
        }

        @Override
        public void doSomeThing() {
            System.out.println(connection != null ? "obj: " + connection.getThisCount() : null);
        }
    }

}

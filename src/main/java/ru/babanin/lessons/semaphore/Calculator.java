package ru.babanin.lessons.semaphore;

import java.util.List;

/**
 * Created by makcim on 10.02.17.
 */
public class Calculator implements Runnable {
    CalcFunc calcFunc;
    int nums[];
    List<Consumer> consumers;

    public Calculator(List<Consumer> consumers, CalcFunc f, int [] nums) {
        this.calcFunc = f;
        this.nums = nums;
        this.consumers = consumers;
    }

    @Override
    public void run() {
        int sum = 0;
        for (int num : nums) {
            sum += calcFunc.func(num);
        }

        for (Consumer consumer : consumers) {
            switch (calcFunc.getType()) {
                case CUBE:
                    consumer.synhForTypeFunction(sum, 0, 0);
                    break;
                case SQUEAR:
                    consumer.synhForTypeFunction(0, sum, 0);
                    break;
                case PROSTATOR:
                    consumer.synhForTypeFunction(0, 0, sum);
                    break;
                default:
                    break;
            }
        }

    }
}

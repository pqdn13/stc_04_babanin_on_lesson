package ru.babanin.lessons.semaphore;

/**
 * Created by makcim on 10.02.17.
 */
public class Consumer {
    volatile boolean lockA;
    volatile boolean lockB;
    volatile boolean lockC;
    int sum = 0;

    void synhForTypeFunction(int a, int b, int c){
        if(a!=0){
            while (lockA){}
            lockA = true;
            System.out.println("a3");
            sum += a;
        }
        if(b!=0){
            while (lockB){}
            lockB = true;
            System.out.println("b3");
            sum += b;
        }
        if(c!=0){
            while (lockC){}
            lockC = true;
            System.out.println("c1");
            sum += c;
        }
        System.out.println(Integer.toString(sum));
        lockA = false;
        lockB = false;
        lockC = false;
    }
}

package ru.babanin.lessons.semaphore;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by makcim on 10.02.17.
 */
public class Main {
    public static void main(String[] args) {
        CalcFunc cube = new CalcFunc() {
            @Override
            public int func(int a) {
                return a * a * a;
            }

            @Override
            public TypeMath getType() {
                return TypeMath.CUBE;
            }
        };

        CalcFunc squear = new CalcFunc() {
            @Override
            public int func(int a) {
                return a * a;
            }

            @Override
            public TypeMath getType() {
                return TypeMath.SQUEAR;
            }
        };

        CalcFunc pros = new CalcFunc() {
            @Override
            public int func(int a) {
                return a;
            }

            @Override
            public TypeMath getType() {
                return TypeMath.PROSTATOR;
            }
        };

        List<Consumer> consumers = new ArrayList<Consumer>();
        consumers.add(new Consumer());
        consumers.add(new Consumer());

        int arg[] = {1, 2, 3, 4, 5, 6};

        List<Thread> list = new ArrayList<>();
        list.add(new Thread(new Calculator(consumers, cube, arg)));
        list.add(new Thread(new Calculator(consumers, squear, arg)));
        list.add(new Thread(new Calculator(consumers, pros, arg)));

        for (Thread thread : list) {
            thread.start();
        }

        for (Thread thread : list) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

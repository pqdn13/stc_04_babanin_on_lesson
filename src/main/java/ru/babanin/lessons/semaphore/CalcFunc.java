package ru.babanin.lessons.semaphore;


public interface CalcFunc {
    int func(int a);
    TypeMath getType();
}

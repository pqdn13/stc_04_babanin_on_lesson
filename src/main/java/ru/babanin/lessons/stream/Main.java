package ru.babanin.lessons.stream;

import java.io.*;
import java.net.URL;
import java.util.Arrays;
import java.util.Objects;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 * Created by makcim on 03.03.17.
 */
public class Main {
    private static final Pattern urlFilePattern = Pattern.compile("[\\w\\.]+:\\/\\/\\S+\\/\\S+");
    private static final Pattern localFilePattern = Pattern.compile("(\\.txt)$");

    public static void main(String[] args) {

        try (Stream<String> stream = Arrays.stream(args).parallel()) {
            int sum = stream
                    .map(Main::path2Stream)
                    .filter(Objects::nonNull)
                    .flatMap((inputStream) -> new BufferedReader(new InputStreamReader(inputStream)).lines())
                    .flatMap(s -> Arrays.stream(s.split(" ")))
                    .mapToInt(s -> Integer.parseInt(s))
                    .filter(i -> i > 0)
                    .filter(i -> i % 2 == 0)
                    .sum();

            System.out.println(sum);
        } catch (Exception e) {
            System.out.println("Что-то пошло не так");
        }

    }

    public static InputStream path2Stream(String source){
        InputStream stream = null;
        if (urlFilePattern.matcher(source).find()) {
            try {
                stream = new URL(source).openStream();
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        } else {
            try {
                stream = new FileInputStream(source);
            } catch (FileNotFoundException e) {
                System.out.println(e.getMessage());
            }
        }

        return stream;
    }
}

package ru.babanin.lessons.xml.lecacy;

/**
 * Created by makcim on 10.02.17.
 */
public class Person {
    private String name;
    private Integer age;
    private Double salary;

    private boolean isChild;

    public Person(String name, Integer age, Double salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
        this.isChild = age < 18;
    }
}

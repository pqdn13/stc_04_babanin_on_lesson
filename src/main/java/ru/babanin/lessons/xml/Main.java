package ru.babanin.lessons.xml;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import ru.babanin.lessons.xml.lecacy.Person;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;

public class Main {
    public static void main(String[] args) throws Exception{
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.newDocument();

        Element root = doc.createElement("persons");
        doc.appendChild(root);

        root.appendChild(createPersonNode(doc, new Person("Max", 33, 100000.0)));
        root.appendChild(createPersonNode(doc, new Person("Liza", 10, 100.0)));
        root.appendChild(createPersonNode(doc, new Person("Oleshka", 12, 0.0)));


        OutputStream stream = new FileOutputStream("out/test.xml");

        printXml(System.out, doc);
        printXml(stream, doc);
    }

    static void printXml(OutputStream out, Document doc) throws Exception{
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(out);
        TransformerFactory transFactory = TransformerFactory.newInstance();
        Transformer transformer = transFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
        transformer.transform(source, result);
    }

    static Element createPersonNode(Document doc, Person person) throws Exception{
        Element node = doc.createElement("person");

        Field[] publicFields = person.getClass().getDeclaredFields();
        for (Field field : publicFields) {
            Element f = doc.createElement("field");

            Class fieldType = field.getType();
            field.toString();

            f.setAttribute("type", fieldType.getName());
            f.setAttribute("name", field.getName());

            field.setAccessible(true);
            Object value = field.get(person);
            f.setAttribute("value", value.toString());

            node.appendChild(f);
        }

        return node;
    }
}

package ru.babanin.lessons.database;

import java.sql.*;

public class Main {
    public static String login = "makcim";
    public static String pasword = "123456";
    public static String urldb = "jdbc:postgresql://localhost:5432/students";

    public static void main(String[] args) {
        System.out.println("org.post");

        try {
            // Регистрация класса драйвера работы с базой
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        try(Connection dbh = DriverManager.getConnection(urldb, login, pasword)) {

            test2(dbh);

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void test1(Connection dbh) {
        try(Statement st = dbh.createStatement()) {
            ResultSet rs = st.executeQuery(
                    "SELECT l.name AS lesson, s.name AS student, j.date as mydate\n" +
                            "FROM journal j\n" +
                            "  JOIN student as s ON (j.id_student = s.id)\n" +
                            "  JOIN lesson as l ON (j.id_lesson = l.id)"
            );

            while (rs.next()) {

                System.out.print(rs.getString(2).trim() + " ");
                System.out.print(rs.getString(1).trim() + " ");
                System.out.println(rs.getString(3).trim() + " ");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void test2(Connection dbh) {
        String request = "SELECT l.name AS lesson, s.name AS student, j.date as mydate\n" +
                "FROM journal j\n" +
                "  JOIN student as s ON (j.id_student = s.id)\n" +
                "  JOIN lesson as l ON (j.id_lesson = l.id)";

        try(PreparedStatement st = dbh.prepareStatement(request)) {
            ResultSet rs = st.executeQuery();

            while (rs.next()) {

                System.out.print(rs.getString(2).trim() + " ");
                System.out.print(rs.getString(1).trim() + " ");
                System.out.println(rs.getString(3).trim() + " ");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

package ru.babanin.lessons.database.model;

public class People implements CommandSql{
  private Long id;
  private String login;
  private Long hash_password;
  private Long access_level;
  private String first_name;
  private String second_name;
  private String email;
  private java.sql.Date birth;
  private java.sql.Date check_in;
  private java.sql.Date last_edit;
  private Long money;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public Long getHash_password() {
    return hash_password;
  }

  public void setHash_password(Long hash_password) {
    this.hash_password = hash_password;
  }

  public Long getAccess_level() {
    return access_level;
  }

  public void setAccess_level(Long access_level) {
    this.access_level = access_level;
  }

  public String getFirst_name() {
    return first_name;
  }

  public void setFirst_name(String first_name) {
    this.first_name = first_name;
  }

  public String getSecond_name() {
    return second_name;
  }

  public void setSecond_name(String second_name) {
    this.second_name = second_name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public java.sql.Date getBirth() {
    return birth;
  }

  public void setBirth(java.sql.Date birth) {
    this.birth = birth;
  }

  public java.sql.Date getCheck_in() {
    return check_in;
  }

  public void setCheck_in(java.sql.Date check_in) {
    this.check_in = check_in;
  }

  public java.sql.Date getLast_edit() {
    return last_edit;
  }

  public void setLast_edit(java.sql.Date last_edit) {
    this.last_edit = last_edit;
  }

  public Long getMoney() {
    return money;
  }

  public void setMoney(Long money) {
    this.money = money;
  }

  @Override
  public String insert2Table() {
    return null;
  }
}

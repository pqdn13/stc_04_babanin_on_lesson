package ru.babanin.lessons.database.model;

public class Status2test implements CommandSql{
  private Long id;
  private Long id_test;
  private String name;
  private Long min_mark;
  private Long max_mark;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getId_test() {
    return id_test;
  }

  public void setId_test(Long id_test) {
    this.id_test = id_test;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getMin_mark() {
    return min_mark;
  }

  public void setMin_mark(Long min_mark) {
    this.min_mark = min_mark;
  }

  public Long getMax_mark() {
    return max_mark;
  }

  public void setMax_mark(Long max_mark) {
    this.max_mark = max_mark;
  }

  @Override
  public String insert2Table() {
    return null;
  }
}

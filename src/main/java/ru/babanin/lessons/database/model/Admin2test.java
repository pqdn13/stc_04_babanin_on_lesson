package ru.babanin.lessons.database.model;

public class Admin2test implements CommandSql{
  private Long id;
  private Long id_test;
  private Long id_user;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getId_test() {
    return id_test;
  }

  public void setId_test(Long id_test) {
    this.id_test = id_test;
  }

  public Long getId_user() {
    return id_user;
  }

  public void setId_user(Long id_user) {
    this.id_user = id_user;
  }

  @Override
  public String insert2Table() {
    return "INSERT INTO admin2test"
            + "(id_test, id_user) VALUES"
            + "(?,?)";
  }
}

package ru.babanin.lessons.database.model;

public class Tests implements CommandSql{
  private Long id;
  private String name;
  private Long access_level;
  private Long hash_password;
  private Long max_mark;
  private Long max_try;
  private Long cost;
  private java.sql.Date birth;
  private java.sql.Date last_edit;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getAccess_level() {
    return access_level;
  }

  public void setAccess_level(Long access_level) {
    this.access_level = access_level;
  }

  public Long getHash_password() {
    return hash_password;
  }

  public void setHash_password(Long hash_password) {
    this.hash_password = hash_password;
  }

  public Long getMax_mark() {
    return max_mark;
  }

  public void setMax_mark(Long max_mark) {
    this.max_mark = max_mark;
  }

  public Long getMax_try() {
    return max_try;
  }

  public void setMax_try(Long max_try) {
    this.max_try = max_try;
  }

  public Long getCost() {
    return cost;
  }

  public void setCost(Long cost) {
    this.cost = cost;
  }

  public java.sql.Date getBirth() {
    return birth;
  }

  public void setBirth(java.sql.Date birth) {
    this.birth = birth;
  }

  public java.sql.Date getLast_edit() {
    return last_edit;
  }

  public void setLast_edit(java.sql.Date last_edit) {
    this.last_edit = last_edit;
  }

  @Override
  public String insert2Table() {
    return null;
  }
}

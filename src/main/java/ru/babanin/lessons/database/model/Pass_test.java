package ru.babanin.lessons.database.model;

public class Pass_test implements CommandSql{
  private Long id;
  private Long id_person;
  private Long id_quest;
  private Long id_test;
  private Long mark;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getId_person() {
    return id_person;
  }

  public void setId_person(Long id_person) {
    this.id_person = id_person;
  }

  public Long getId_quest() {
    return id_quest;
  }

  public void setId_quest(Long id_quest) {
    this.id_quest = id_quest;
  }

  public Long getId_test() {
    return id_test;
  }

  public void setId_test(Long id_test) {
    this.id_test = id_test;
  }

  public Long getMark() {
    return mark;
  }

  public void setMark(Long mark) {
    this.mark = mark;
  }

  @Override
  public String insert2Table() {
    return "INSERT INTO admin2test"
            + "(id_test, id_user) VALUES"
            + "(?,?)";
  }
}

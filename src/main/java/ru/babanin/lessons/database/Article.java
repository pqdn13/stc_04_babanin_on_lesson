package ru.babanin.lessons.database;

import java.util.Date;

/**
 * Created by makcim on 17.02.17.
 */
public class Article {
    String lesson;
    String student;
    Date date;

    public Article() {
    }

    public Article(String lesson, String student, Date date) {
        this.lesson = lesson;
        this.student = student;
        this.date = date;
    }


    public String getLesson() {
        return lesson;
    }

    public void setLesson(String lesson) {
        this.lesson = lesson;
    }

    public String getStudent() {
        return student;
    }

    public void setStudent(String student) {
        this.student = student;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return lesson + " " + student + " " + date;
    }
}

package ru.babanin.lessons.Test;

public class A extends C{

    {
        System.out.println("init block");
    }

    final B b = new B();

    public A() {
        System.out.println("A()");
    }
}

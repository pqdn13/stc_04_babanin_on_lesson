package ru.babanin.lessons.Test;

class Class1 {
    Class1(int i) {
        System.out.println("Class1(int)");
    }
}

public class Main extends Class1 {
    Main(double d) {              // 1
        this((int) d);
        System.out.println("Main(double)");
    }

    Main(int i) {                 // 2
        super(i);
        System.out.println("Main(int)");
    }

    public static void main(String[] args) {
        String s1 = "str";
        String s2 = "str";
        System.out.println("Result: " + s1 == s2);
    }
}



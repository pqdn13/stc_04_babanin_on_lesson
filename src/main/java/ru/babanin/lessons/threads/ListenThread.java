package ru.babanin.lessons.threads;

/**
 * Created by makcim on 09.02.17.
 */
public class ListenThread implements Runnable {
    private boolean flag = false;
    private final int maxCount;
    private int count;

    public ListenThread(int count) {
        this.maxCount = count;
        this.count = 0;
    }

    @Override
    public void run() {
        try {
            while (true) {
                synchronized (this) {
                    while (!flag) {
                        this.wait();
                    }
                    flag = false;
                }

                count++;
                if (count == maxCount) {
                    count = 0;
                    System.out.println("Next " + Integer.toString(maxCount) + " sec");
                }
            }
        } catch (InterruptedException e) {
            System.out.println("trash");
        }
    }

    public synchronized void tic(){
        flag = true;
        notify();
    }
}

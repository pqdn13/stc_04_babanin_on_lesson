package ru.babanin.lessons.threads;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by makcim on 09.02.17.
 */
public class Test {

    static List<Thread> threads = new ArrayList<>();
    static List<ListenThread> listeners = new ArrayList<>();


    public static void main(String[] args) {

        Thread aThread = new Thread(()->{
            try {
                long start = System.currentTimeMillis();
                while (true) {
                    Thread.sleep(1000);
                    Test.incSec();
                }

            } catch (InterruptedException e) {

            }
        });

        listeners.add(new ListenThread(1));
        listeners.add(new ListenThread(4));
        listeners.add(new ListenThread(7));

        for (ListenThread listener : listeners) {
            Thread thread = new Thread(listener);
            threads.add(thread);
            thread.start();
        }
        
        aThread.start();

        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    public static void incSec(){
        System.out.println("Send tic");
        for (ListenThread listener : listeners) {
            listener.tic();
        }
    }
}

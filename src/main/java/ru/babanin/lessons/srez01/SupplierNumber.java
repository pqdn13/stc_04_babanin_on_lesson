package ru.babanin.lessons.srez01;

import java.util.Random;
import java.util.function.Consumer;

/**
 * Created by makcim on 13.02.17.
 */
public class SupplierNumber implements Runnable {
    BoxFlag stopFlag;
    Consumer<Integer> consumer;

    public SupplierNumber(BoxFlag stopFlag, Consumer<Integer> consumer) {
        this.stopFlag = stopFlag;
        this.consumer = consumer;
    }

    @Override
    public void run() {
        Random random = new Random();
        while (!stopFlag.isFlag()) {
            consumer.accept(Math.abs(random.nextInt() % 100));
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

package ru.babanin.lessons.srez01;

/**
 * Created by makcim on 13.02.17.
 */
public class Main {
    public static void main(String[] args) {

        BoxFlag stopFlag = new BoxFlag();

        ConsumerNumber consumerNumber = new ConsumerNumber(stopFlag);
        SupplierNumber supplierNumber = new SupplierNumber(stopFlag, consumerNumber);

        Thread thread1 = new Thread(consumerNumber);
        Thread thread2 = new Thread(supplierNumber);

        thread1.start();
        thread2.start();

        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

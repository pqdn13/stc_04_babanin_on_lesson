package ru.babanin.lessons.srez01;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.*;

/**
 * Created by makcim on 13.02.17.
 */
public class ConsumerNumber implements Runnable, Consumer<Integer> {
    BoxFlag stopFlag;
    Map<Integer, Integer> map = new ConcurrentHashMap<>();


    public ConsumerNumber(BoxFlag stopFlag) {
        this.stopFlag = stopFlag;
    }

    @Override
    public void run() {

        do{
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
                System.out.println("" + entry.getKey() + " : " + entry.getValue());
            }

        }while (!stopFlag.isFlag());

    }

    @Override
    public void accept(Integer num) {
        if (map.containsKey(num)) {
            int count = map.get(num) + 1;
            if(count >= 5) stopFlag.setFlag();
            map.put(num, count);
        }else{
            map.put(num, 1);
        }
    }
}

package ru.babanin.lessons.srez01;

/**
 * Created by makcim on 13.02.17.
 */
public class BoxFlag {
    boolean flag;

    public BoxFlag() {
        this.flag = false;
    }

    public boolean isFlag() {
        return flag;
    }

    public synchronized void setFlag() {
        this.flag = true;
    }
}

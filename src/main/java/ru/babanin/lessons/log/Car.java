package ru.babanin.lessons.log;

/**
 * Created by makcim on 14.02.17.
 */
public class Car {
    private long price;
    private String model;
    private String regNum;

    public Car(long price, String model, String regNum) {
        this.price = price;
        this.model = model;
        this.regNum = regNum;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getRegNum() {
        return regNum;
    }

    public void setRegNum(String regNum) {
        this.regNum = regNum;
    }
}

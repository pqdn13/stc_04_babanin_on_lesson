package ru.babanin.lessons.log;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

public class Main {
    private static final Logger LOGGER = Logger.getLogger(Main.class);

    static {
        DOMConfigurator.configure("src/main/resources/log4j.xml");
    }

    public static void main(String[] args) {
        LOGGER.error(new Car(100L, "Yamaha", "s122kl"));
    }
}

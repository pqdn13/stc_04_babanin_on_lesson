package ru.babanin.lessons.log;

import org.apache.log4j.PatternLayout;
import org.apache.log4j.spi.LoggingEvent;

import java.lang.reflect.Field;

/**
 * Created by makcim on 14.02.17.
 */
public class MailLogLayout extends PatternLayout {
    @Override
    public String format(LoggingEvent event) {
        String out = super.format(event);

        if (event.getMessage() instanceof String) {
            return out;
        }

        String[] params = out.split(" ");
        StringBuilder builder = new StringBuilder(out.length());

        Object obj = event.getMessage();

        StringBuilder builderObject = new StringBuilder();
        builderObject.append("[Object ");
        builderObject.append(obj.getClass().getName());
        builderObject.append(":");

        Field[] publicFields = obj.getClass().getDeclaredFields();
        for (Field field : publicFields) {

            builderObject.append(" ");
            builderObject.append(field.getName());

            field.setAccessible(true);
            builderObject.append("=\"");
            try {
                builderObject.append(field.get(obj).toString());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            builderObject.append("\";");
        }

        builderObject.append("]");
        params[params.length - 1] = builderObject.toString();

        for (String param : params) {
            builder.append(param);
        }

        return builder.toString();
    }
}
